<?php

namespace A4Sex;

abstract class AbstractSignedToken
{
    const TOKEN_REGEXP = '/[[:alnum:]\-+=_]{1,40}\.\d+\.[[:alnum:]]{32}/';

    public int $tokenLifetime = 60;

    protected string $secret;
    protected int $time;
    protected ?string $id = null;
    protected bool $bypass = false;

    public function __construct(?int $tokenLifetime = null, ?string $secret = null)
    {
        $this->setTtl($tokenLifetime);
        $this->setSecret($secret);
        $this->setCreated(time());
    }

    public function setCreated(?int $time = null): void
    {
        if ($time) {
            $this->time = $time;
        }
    }

    public function setTtl(?int $time = null): void
    {
        if ($time) {
            $this->tokenLifetime = $time;
        }
    }

    public function created(): ?int
    {
        return $this->time;
    }

    public function setSecret(?string $secret = null): void
    {
        if (!$secret) {
            return;
        }
        $this->secret = $secret;
    }

    public function setBypass(bool $value): void
    {
        $this->bypass = ($value === true);
    }

    public function getId(?string $token = null)
    {
        if ($token) {
            return $this->parse($token)[0];
        }
        return $this->id;
    }

    public function test(string $token): bool
    {
        if (preg_match(self::TOKEN_REGEXP, $token)) {
            return true;
        }
        return false;
    }

    public function parse(string $token): array
    {
        if (!$this->test($token)) {
            throw new \TypeError('This sting does not match the token format');
        }
        return explode('.', $token);
    }

    public function generateId(string $prefix = ''): string
    {
        return md5($this->secret . $prefix . uniqid(mt_rand(1111, 8888), true));
    }

    public function sign(string $id, int $time): string
    {
        return md5(join(':', [
            $id,
            $time,
            $this->secret,
        ]));
    }

    public function expire(int $created): int
    {
        return $created + $this->tokenLifetime;
    }

    abstract public function create($id = null, $expire = null): string;

    abstract public function expired(int $expire, $ignore = false): bool;

    public function signed(string $token, bool $ignore = false): bool
    {
        if ($ignore) {
            return true;
        }
        if ($token) {
            list($id, $time, $sign) = $this->parse($token);
            if ($sign === $this->sign($id, $time)) {
                return true;
            }
        }
        return false;
    }

    public function valid(?string $token, bool $ignoreSign = false, bool $ignoreExpires = false)
    {
        if (!$token or !$this->test($token)) {
            return false;
        }
        [$id, $time] = $this->parse($token);
        $expired = $this->expired($time, $this->bypass ?? $ignoreExpires);
        $signed = $this->signed($token, $this->bypass ?? $ignoreSign);
        if ($id and $signed and !$expired) {
            return $id;
        }
        return false;
    }
}
