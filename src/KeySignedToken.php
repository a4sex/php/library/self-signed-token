<?php

namespace A4Sex;

class KeySignedToken extends SelfSignedToken
{
    const TOKEN_REGEXP = '/[[:alnum:]\-+=_]{1,40}\.\d+\.[[:alnum:]]{32}\.[[:alnum:]]{1,40}/';

    protected array $keys = [];

    protected ?string $keyId = null;

    public function __construct(?int $tokenLifetime = null, ?string $secret = null, ?string $keyId = null)
    {
        parent::__construct($tokenLifetime, $secret);
        $this->setKey($keyId, $secret);
        $this->useKey($keyId);
    }

    public function setKeys($keys = []): void
    {
        foreach ($keys as $keyId => $secret) {
            $this->setKey($keyId, $secret);
        }
    }

    public function setKey(?string $keyId = null, ?string $secret = null): void
    {
        if (!$keyId or !$secret) {
            return;
        }
        $this->keys[$keyId] = $secret;
    }

    public function hasKey(?string $keyId = null): bool
    {
        if (!$keyId or !array_key_exists($keyId, $this->keys)) {
            return false;
        }

        return true;
    }

    public function useKey(string $keyId, bool $strict = false): bool
    {
        if (!$this->hasKey($keyId)) {
            if ($strict) {
                throw new \TypeError(sprintf('This key does not exist: %s', $keyId));
            }
            return false;
        }
        $this->keyId = $keyId;
        $this->setSecret($this->keys[$keyId]);

        return true;
    }

    public function sign(string $id, int $time): string
    {
        return md5(join(':', [
            $id,
            $time,
            $this->keyId,
            $this->secret,
        ]));
    }

    public function signed(string $token, bool $ignore = false): bool
    {
        if ($ignore) {
            return true;
        }
        if ($token) {
            list($id, $time, $sign, $keyId) = $this->parse($token);
            if (!$this->hasKey($keyId)) {
                return false;
            }
            $this->useKey($keyId);
            if ($sign === $this->sign($id, $time)) {
                return true;
            }
        }
        return false;
    }

    public function create($id = null, $expire = null): string
    {
        $this->id = $id ?: $this->generateId();
        $expire = $expire ?: $this->expire($this->time);
        return join('.', [
            $this->id,
            $expire,
            $this->sign($this->id, $expire),
            $this->keyId,
        ]);
    }

}
