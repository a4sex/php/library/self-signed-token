<?php

use A4Sex\KeySignedToken;
use PHPUnit\Framework\TestCase;

class KeySignedTokenTest extends TestCase
{
    public KeySignedToken $token;

    public function setUp(): void
    {
        $this->token = new KeySignedToken(10, '12345', 'abcde');
    }

    public function testSigned()
    {
        $token = $this->token->create();
        $this->assertTrue($this->token->signed($token));
    }

    public function testGetId()
    {
        $this->assertNull($this->token->getId());

        $value = '6e000eeabea27aa13a0476d656e5a15e.1560148598.70272e700a46b3040ee53f2b083e3875';
        $this->assertEquals('6e000eeabea27aa13a0476d656e5a15e', $this->token->getId($value));
    }

    public function testCreate()
    {
        $this->assertNotEmpty($this->token->create());
    }

    public function testValid()
    {
        $token = $this->token->create();
        $this->assertNotNull($this->token->valid($token));
    }

    public function testParse()
    {
        $token = $this->token->create();
        $this->assertCount(4, $this->token->parse($token));
    }

    public function testGenerate()
    {
        $this->assertMatchesRegularExpression('/^[a-f0-9]{32}$/', $this->token->generateId());
    }

    public function testSign()
    {
        $id = '4c28afda06a25a53f2c1a8ddc491edd5';
        $value = 'a6d2bfcb43ac40d656f036f458e13658';
        $this->assertEquals($value, $this->token->sign($id, '1560150024'));
    }

    public function testExpired()
    {
        $token = new KeySignedToken(10, '12345', 'abcde');
        [$id, $expire] = $token->parse($token->create());
        $created = $token->created();
        $this->assertTrue($token->expired($created));

        $this->assertFalse($token->expired($expire - 1));
        $this->assertFalse($token->expired($expire - 5));
        $this->assertFalse($token->expired($expire - 9));

        $this->assertTrue($token->expired($expire - 10));
        $this->assertTrue($token->expired($expire - 15));
        $this->assertTrue($token->expired('1456789023'));
    }

    public function testSetKey()
    {
        $tokenOld = $this->token->create();
        $this->token->setKey('wxyz', '98765');
        $tokenNew = $this->token->create();
        $this->assertTrue($this->token->signed($tokenNew));
        $this->assertTrue($this->token->signed($tokenOld));
    }

    public function testUseKey()
    {
        $this->token = new KeySignedToken(10, '12345', 'abcd');
        $this->token->setKey('wxyz', '98765');
        $tokenOld = $this->token->create();
        print_r([$tokenOld]);
        $this->token->useKey('wxyz');
        $tokenNew = $this->token->create();
        print_r([$tokenNew]);
        $this->assertTrue($this->token->signed($tokenNew));
        $this->assertTrue($this->token->signed($tokenOld));
        $this->token->useKey('abcd');
        $this->assertTrue($this->token->signed($tokenOld));
        $this->assertTrue($this->token->signed($tokenNew));
    }
}
